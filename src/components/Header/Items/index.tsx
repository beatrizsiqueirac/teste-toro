import React from 'react'
import { IoIosArrowBack } from 'react-icons/io'
import { clearSession } from '../../../services/auth'

const HeaderHomeProperties = (props: { history: string[] }) => {
  const name = JSON.parse(localStorage.getItem("@userData") || "").name;
  const stringName = name === "" ? "" : `${name}`;
    return (
        <div className='menu-items'>
            <div className='group-header'>
                <button
                    className='icon'
                    onClick={() => {
                        clearSession()

                        props.history.push('/')
                    }}
                >
                    <IoIosArrowBack size={18} color='#00AEED' />
                    <p>Sair</p>
                </button>
            </div>
            <div className='group-header'>                
                <p className='item-header'>Olá, {stringName}</p>
            </div>

        </div>
    )
}

export default HeaderHomeProperties