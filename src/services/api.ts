import axios from "axios";
import { AuthResponse } from "../interfaces/user";
import { setUserDataOnLocalStorage } from "../utils/user";
import { clearSession } from "./auth";

const baseUrl = "http://localhost:3333";

export const publicApi = axios.create({
    baseURL: baseUrl,
});

export const privateApi = axios.create({
    baseURL: baseUrl,
    headers: {
        authorization: localStorage.getItem("@userSession") || "invalid",
    },
});

privateApi.defaults.validateStatus = (status: number) => {
    if (status >= 400) {
        return false;
    }
    return true;
};

export const logIn = async (
    email: string,
    password: string
): Promise<AuthResponse> => {
    clearSession();

    const apiResponse = await publicApi.post("/api/users/authenticate", {
        email,
        password,
    });

    setUserDataOnLocalStorage(apiResponse.data);
    privateApi.defaults.headers.authorization = apiResponse.data.token;

    return apiResponse.data;
};
