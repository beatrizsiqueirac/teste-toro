import { Route, Redirect } from "react-router-dom"
import { isUserAutheticated } from "../services/auth"

export const PrivateRoute = ({ Component, ...rest }: any) => {
    return (
        <Route {...rest} render={(props) => {
            return (
                isUserAutheticated() ? (
                    <Component {...props} />
                ) : (
                        <Redirect to="/unauthorized" />
                    )
            )
        }} />
    )
}