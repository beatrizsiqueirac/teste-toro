import ToroHome from "../../assets/ToroHome.png";
import Header from "../../components/Header";
import { RiCommunityFill } from "react-icons/ri";
import Grid from "@material-ui/core/Grid";
import HeaderItemsProperties from "../../components/Header/Items";

const Home = (props: { history: string[] }) => {    
  const name = JSON.parse(localStorage.getItem("@userData") || "").name;
  const stringName = name === "" ? "" : `${name}`;
  return (
    <Grid
      container
      direction="column"
      justify="center"
      alignItems="center"
      spacing={3}
    >
      <Header
        Component={HeaderItemsProperties}
        componentProps={{ history: props.history }}
      />
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        spacing={3}
        className="form-content"
      >
        <Grid item>
          <img src={ToroHome} alt="home"></img>
        </Grid>
        <Grid item className="form-description">
          <RiCommunityFill size={150} className="icon"></RiCommunityFill>
          <h2>{`Olá, ${stringName}!`}</h2>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Home;
